/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author laercio
 */
public class Adicional extends Produto{
    private Produto produto;

    public Adicional(Produto produto, String nome, float valor) {
        super(nome, valor);
        this.produto = produto;
    }

    public Adicional(String nome, float valor) {
        super(nome, valor);
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }
    
    
    public String getNome(){
        return produto.getNome()+", "+this.nome;
    }
    
    public float getValor(){
        System.out.println(produto.getNome());
        return produto.getValor()+ this.valor;
    }
    
    
    
    
}
