/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.ArrayList;

/**
 *
 * @author laercio
 */
public class Pedido {
    private int id;
    private Cliente cli = new Cliente();
    private ArrayList<Produto> produtos = new ArrayList<>();
    
    
    public void addProduto(Produto p){
        produtos.add(p);
    }

    public Pedido(int id) {
        this.id = id;
    }
    
    

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    @Override
    public String toString() {
        return "Pedido "+id;
    }
    
    public float getTotal(){
        float total = 0;
        
        for (Produto produto : produtos) {
            total += produto.getValor();
        }
        
        return total;
    }

    public Cliente getCli() {
        return cli;
    }

    public void setCli(Cliente cli) {
        this.cli = cli;
    }
    
    
    

    
    
    
    
    
}
