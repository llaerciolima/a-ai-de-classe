/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import beans.Adicional;
import beans.Cliente;
import beans.Pedido;
import beans.Produto;
import java.util.ArrayList;

/**
 *
 * @author laercio
 */
public class PedidoController {

    public ArrayList<Pedido> listar() {
        ArrayList<Pedido> pedidos = new ArrayList<>();
        Pedido p = new Pedido(10);
        Cliente c = new Cliente();
        c.setNome("Joao da Silva");
        c.setEndereco("Rua das flores rosadas n5");
        c.setTelefone("9988-4512");
        p.setCli(c);
        Produto pr = new Produto("Açai na tigle", 10);
        Adicional a = new Adicional(pr, "Paçoca", 1);
        a = new Adicional(a, "Kiwi", 3);
        
        p.addProduto(a);
        
        pedidos.add(p);
        pedidos.add(new Pedido(11));
        pedidos.add(new Pedido(12));
        pedidos.add(new Pedido(13));
        pedidos.add(new Pedido(14));
        pedidos.add(new Pedido(15));
        
        return pedidos;
    }

    public Pedido listarPorId(int id) {

        return new Pedido(10);
    }

    public boolean add(Pedido p) {
        
        System.out.println("");
        System.out.println("");
        for (Produto pr : p.getProdutos()) {
            
            System.out.println(pr.getNome()+"- - - - "+pr.getValor());
        }
        return true;
    }

    public boolean editar(Pedido p) {
        return true;
    }

    public boolean remover(Pedido p) {
        return true;
    }
}
