/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;



/**
 *
 * @author laercio
 */
public abstract class JanelaFactory {

    public static Janela getJanela(String nome, Object param) {
        System.out.println("aqui");
        try {
            Class c = Class.forName("views."+nome);
            
            Object object = c.getConstructor(Object.class).newInstance(param);
            
            return (Janela) object;

        } catch (Exception e) {
            System.out.println("Janela nao encontrada");
        }

        return null;
    }

}
